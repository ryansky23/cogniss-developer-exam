const jsonfile = require("jsonfile");
const randomstring = require("randomstring");

const inputFile = "./input2.json";
const outputFile = "./output2.json";

var output = {};
let generateRandomChars = randomstring.generate({
    length: 5,
    charset: "alphabetic"
});
try {
    jsonfile.readFile(inputFile, function (err, body) {
        let names = body.names;
        output.emails = [];
        output.emails = names.map((name) => (name.split("").reverse().join("").concat(generateRandomChars, "@gmail.com").toLowerCase()));
        console.log("output: ", output);

        jsonfile.writeFile(outputFile, output, { spaces: 2 })
            .then(res => {
                console.log(`${outputFile} is written successfully.`)
            })
            .catch(err => console.error("err: ", err))
            ;
    });
} catch (error) {
    console.log("err: ", error)
};




























